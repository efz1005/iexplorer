# Isogeny Database Website

To build the website, install [eleventy](https://www.11ty.dev/) and run

```
eleventy
```

Note that you need to have a local copy of the database. Further automatization will be added in the near future.